table member
    member_id(PK)
    member_name
    password

table message
    message_id(PK)
    content
    time
    sender_id(FK)
    receiver_id(FK)

table picture
    url(PK)
    member_id(FK)

db
create table member(
	member_id int not null,
    password int not null,
    member_name varchar(256) not null,
    primary key(member_id)
);
drop table member;

create table picture(
	url varchar(200) not null,
    member_id int not null,
    primary key(url),
    foreign key(member_id) references member(member_id)
);
drop table picture;

create table message(
	message_id int not null auto_increment,
    message_time datetime not null,
    content varchar(255),
    sender_id int not null,
    receiver_id int not null,
    primary key(message_id),
    foreign key(sender_id) references member(member_id),
	foreign key(receiver_id) references member(member_id),
);
drop table message;



API

1. POST "/register"
receive
    member_id
    password
    member_name
return
    success
    token(set to localStorage)
    message

2. POST "/login"
receive
    member_id
    password
return 
    success;
    token(set to localStorage)
    message;

3. DELETE "/logout"
return
    success
    message
    (delete token from localStorage)

4. POST "'/send/:senderId/:receiverId'" (use socket)
receive
    message_id
    sender_id
    receiver_id
    content
    time
return 
    success
    message
    sender_id
    receiver_id
    content
    time

5. POST "/receive/:receiverId/:senderId/" (use socket)
receive
    message_id
    sender_id
    receiver_id
    content
    time
return 
    success
    message
    sender_id
    receiver_id
    content
    time

6. GET "/member/conversation/:member2Id"
receive
    member_id 1
    member_id 2
return
    success
    message
    receiver_id
    sender_id
    content
    time

7. GET "/search?member=memberId"
return
    success
    message

8. GET "/member/friendList" 
receive
    memberId
return
    success
    message
    last_message_time
    last_message_content
    sender_id
    receiver_id
















