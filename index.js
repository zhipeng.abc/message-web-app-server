const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const saltRounds = 12;
const jwt = require("jsonwebtoken");
const secretCode = "i love you";
const mysql = require("mysql2");
// const port = 5000;
const port = process.env.PORT || 5000;
const db = require("./db");
const cors = require("cors");

app.use(cors());

const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, {
    cors: {
        "origin": "*",
    }
});


let socketList = {};
io.on("connection", (socket) => {

    socket.on("saveMySocket", (id) => {
        socketList[id] = socket;
    });

    socket.on("sendMessage", (msgInfo) => {
        try {
            let token = msgInfo.token
            jwt.verify(token, secretCode, (err, decoded) => {
                const senderId = decoded.memberId;
                const receiverId = msgInfo.member2Id;
                const messageTime = new Date();
                const content = msgInfo.content;

                const conversationId = [senderId, receiverId].sort().join("_");

                //post message to server
                db.getDb().execute(
                    "insert into message (message_time, content, sender_id, receiver_id, conversation_id) value(?,?,?,?,?)", [messageTime, content, senderId, receiverId, conversationId]
                ).then((result) => {
                    if(socketList[receiverId]){

                        socketList[receiverId].emit("receiveMsg", {content: content,  sender: senderId, messageTime: messageTime})
                    }
                })
            })
        } catch (error) {
            console.log(error)
        }
    });
})



app.use(express.json())

app.use("/member", function (req, res, next) {
    let token = req.headers["authorization"].split(" ")[1];
    jwt.verify(token, secretCode, (err, decoded) => {
        if (decoded !== null) {
            req.memberId = decoded.memberId;
            return next();
        } else {
            return res.json({
                success: false,
                message: "token is not valid"
            })
        }
    })
})

app.post("/register", async (req, res) => {
    try {
        const memberId = parseInt(req.body.memberId);
        const password = parseInt(req.body.password);

        //check member is duplicate
        const [memberIdList, c] = await db.getDb().execute(
            "select member_id from member"
        )
        for (let i = 0; i < memberIdList.length; i++) {
            if (memberIdList[i].member_id === memberId) {
                return res.json({
                    success: false,
                    message: "sorry, your member id is used by others"
                })
            }
        }
        //check member id length
        if (memberId.toString().length < 4) {
            return res.json({
                success: false,
                message: "your member ID should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        //check member id is integer
        const memberIdArray = memberId.toString().split("");
        const isAllMemberIdInteger = memberIdArray.every((elem) => {
            return elem.charCodeAt() >= 48 && elem.charCodeAt() <= 57;
        })
        if (isAllMemberIdInteger === false) {
            return res.json({
                success: false,
                message: "your member ID should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        //check password length
        if (password.toString().length < 4) {
            return res.json({
                success: false,
                message: "you password should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        //check password is integer
        const passwordArray = password.toString().split("");
        const isAllPasswordInteger = passwordArray.every((elem) => {
            return elem.charCodeAt() >= 48 && elem.charCodeAt() <= 57;
        })
        if (isAllPasswordInteger === false) {
            return res.json({
                success: false,
                message: "your password should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        //create jwt
        const payload = {
            "memberId": memberId,
        }
        const token = jwt.sign(payload, secretCode);
        //hash password
        let hashPassword = await bcrypt.hash(password.toString(), saltRounds);
        //insert new member to db
        const [result, fields] = await db.getDb().execute(
            "insert into member (member_id, password) value(?, ?)", [memberId, hashPassword]
        );
        if (result.affectedRows > 0) {
            return res.json({
                success: true,
                message: `congratulation, ${memberId}, your registration is successful`,
                token: token
            })
        }
    } catch (error) {
        console.log(error)
    }
})




app.post("/login", async (req, res) => {
    let memberId = req.body.memberId.toString();
    let password = req.body.password.toString();
    try {
        if (memberId.toString().length < 4) {
            return res.json({
                success: false,
                message: "your member ID should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        //check member id is integer
        const memberIdArray = memberId.toString().split("");
        const isAllMemberIdInteger = memberIdArray.every((elem) => {
            return elem.charCodeAt() >= 48 && elem.charCodeAt() <= 57;
        })
        if (isAllMemberIdInteger === false) {
            return res.json({
                success: false,
                message: "your member ID should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        //check password length
        if (password.toString().length < 4) {
            return res.json({
                success: false,
                message: "you password should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        //check password is integer
        const passwordArray = password.toString().split("");
        const isAllPasswordInteger = passwordArray.every((elem) => {
            return elem.charCodeAt() >= 48 && elem.charCodeAt() <= 57;
        })
        if (isAllPasswordInteger === false) {
            return res.json({
                success: false,
                message: "your password should not less than 4 characters and every character should be integer(0-9)"
            })
        }
        const [result, fields] = await db.getDb().execute("select*from member where member_id=?", [memberId]);
        if (result.length === 1) {
            bcrypt.compare(password, result[0].password).then((correct) => {
                if (correct) {
                    const payload = {
                        "memberId": memberId
                    };
                    const token = jwt.sign(payload, secretCode);
                    return res.json({ success: true, token: token, message: 'Welcome Back' })

                } else {
                    res.json({ success: false, message: "password is not correct" })
                }
            })
        } else {
            res.json({ success: false, message: "account not found" })
        }
    } catch (error) {
        console.log(error)
    }
})


app.get("/member/conversation/:conversationId", async (req, res) => {
    try {
        const member1Id = req.memberId
        // const member2Id = req.params.member2Id;
        const conversationId = req.params.conversationId;
        //search all message for two this member
        const [result, fields] = await db.getDb().execute(
            // "select * from message where (sender_id = ? and receiver_id = ?) OR (receiver_id = ? and sender_id = ?)", [member1Id, member2Id, member1Id, member2Id]);
            "select * from message where conversation_id = ?", [conversationId]);
        if (result.length < 0) {
            return res.json({
                success: false,
                message: "he/she is your friend, so there are not any message",
                conversation: result
            })
        } else {
            return res.json({
                success: true,
                message: "success to fetch all message",
                conversation: result
            })
        }
    } catch (error) {
        console.log(error)
    }
})

app.get("/search", async (req, res) => {
    try {
        const memberId = req.query.memberId;
        //find member
        const [result, fields] = await db.getDb().execute(
            "select * from member where member_id = ?", [memberId]
        )
        if (result.length === 0) {
            return res.json({
                success: false,
                message: "can not found the member, please confirm the member ID"
            })
        } else {
            return res.json({
                success: true,
                message: "success for found the member",
                member: result[0]
            })
        }
    } catch (error) {
        console.log(error)
    }
})



app.post('/member/send/:receiverId', async (req, res) => {
    try {
        const senderId = req.memberId
        const receiverId = req.params.receiverId;
        const messageTime = new Date();
        const content = req.body.content;

        const conversationId = [senderId, receiverId].sort().join("_");

        //search there is the member
        const [result2, fields2] = await db.getDb().execute(
            "select * from member where member_id = ?", [receiverId]
        );
        if(!result2){
            return res.json({
                success: false,
                message: "can't found the member Id, so you can not send message"
            })
        }

        //post message to server
        const [result, fields] = await db.getDb().execute(
            "insert into message (message_time, content, sender_id, receiver_id, conversation_id) value(?,?,?,?,?)", [messageTime, content, senderId, receiverId, conversationId]
        );
        if (result.affectedRows > 0) {
            return res.json({
                success: true,
                message: "success for post message to server",
                messageDetail: {
                    messageId: result.insertId,
                    messageTime: messageTime,
                    content: content,
                    senderId: senderId,
                    receiverId: receiverId
                }
            })
        } else {
            return res.json({
                success: false,
                message: "fail for post message to server",
            })
        }
    } catch (error) {
        console.log(error)
    }
});



app.get('/member/receive/:receiverId/:senderId', async (req, res) => {
    let senderId = req.params.senderId;
    let receiverId = req.params.receiverId;
    try {
        const [result, fields] = await db.getDb().execute("select content,message_time from message where receiver_id=? and sender_id=?", [receiverId, senderId])
        return res.json({ success: true, message: "your message is received", result: result })
    } catch (error) {
        console.log(error)
    }
});

app.get("/member/friendList", async (req, res) => {
    try {
        const memberId = req.memberId

        const [result, fields] = await db.getDb().execute(
            "select * from message  join member on member.member_id=message.receiver_id where exists(select max(message_id) as last_message_id from message where sender_id = ? or receiver_id = ?  group by conversation_id having last_message_id=message_id)", [memberId, memberId])

        if (result.length < 0) {
            return res.json({
                success: false,
                message: "fail to get friend list"
            })
        } else if (result.length === 0) {
            return res.json({
                success: true,
                message: "you are new member so there is not friend list",
                friendList: result
            })
        } else if (result.length > 0) {
            return res.json({
                success: true,
                message: "success to get friend list",
                friendList: result
            })
        }
    } catch (error) {
        console.log(error);
    }
});

app.get("/endpoint", async (req, res) => {
    // res.json({"say":"hi"});
    try {
      const [result] = await db.getDb().query("SELECT 12 as sol");
      res.json(result);
    } catch (error) {
        console.log(error)
      res.json({ error: error });
    }
  });



server.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
    const mysqlDb = mysql.createPool({
        host: "database-1.cjvivrqvhvox.ap-southeast-1.rds.amazonaws.com",
        port: 3306,
        user: "admin",
        password: "rootroot",
        database: "message_web_app_db"
    });
    db.setDb(mysqlDb.promise());
})