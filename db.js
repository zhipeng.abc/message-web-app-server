let db = null;

function setDb(d){
    db = d;
}

function getDb(){
    return db;
}

module.exports = {
    setDb,
    getDb
}